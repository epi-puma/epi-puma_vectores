const { gql } = require('apollo-server');


const schema = gql`
  extend type Query {

    echo_vectores(message: String): String,
    all_vectores_covariables(limit: Int!, filter: String!): [CovariableVectores!]!,
    ocurrence_vectores_by_id_covariable(id_covariable: Int!, filter: String!): [OcurrenceVectores!]!,
  	get_diseases_vectores: [Disease]
  	occurrences_by_taxon_vectores(query: String!): [OcurrenceVectores!]!

  }
`;

module.exports = schema;