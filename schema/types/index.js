const ocurrence_type = require('./ocurrence_type');
const covariable_type = require('./covariable_type');
const disease_type = require('./disease_type');

module.exports = [
	ocurrence_type,  
	covariable_type,
	disease_type,
];
